<?php // @author : Jeremie B.

// en attentes de plus d'information pour le finir complétement

// Afficher le partenaire selectionné (auteur : vicent)



function MODELE_getstructureclientid($clientid) {
    global $bdd;
    $reqSQL='SELECT * FROM api_client_grants
    WHERE client_id = :id';

    $requete = $bdd->prepare($reqSQL);
    $requete->bindValue(':id', $clientid, PDO::PARAM_STR);
    $requete->execute();

    $leTuple = $requete->fetchAll();
    $requete->closeCursor();

    return $leTuple;
}
// Afficher toutes les structures 

// Afficher les structures par install_id et branch_id

function MODELE_getBranch($branch_id,$install_id, $client_id) {
    global $bdd;

    $reqSQL="SELECT * 
        FROM api_client_grants
        WHERE branch_id like concat('%',:branch_id,'%')
        AND install_id like concat('%',:install_id,'%')
        AND client_id = :client_id";

    $requete = $bdd->prepare($reqSQL);
    $requete->bindValue(':branch_id', $branch_id, PDO::PARAM_INT);         
    $requete->bindValue(':install_id', $install_id, PDO::PARAM_INT);
    $requete->bindValue(':client_id', $client_id, PDO::PARAM_STR);
    $requete->execute();

    $allbranch = $requete->fetchAll();
    $requete->closeCursor();

    return $allbranch;
}
// Afficher les structures par install_id

function MODELE_getInstall($install_id, $client_id) {
    global $bdd;

    $reqSQL="SELECT * 
        FROM api_client_grants
        WHERE install_id like concat('%',:install_id,'%')
        AND client_id = :client_id";

    $requete = $bdd->prepare($reqSQL);         
    $requete->bindValue(':install_id', $install_id, PDO::PARAM_INT);
    $requete->bindValue(':client_id', $client_id, PDO::PARAM_STR);
    $requete->execute();

    $allbranch = $requete->fetchAll();
    $requete->closeCursor();

    return $allbranch;
}

// afficher les structures actives 

function MODELE_getActive($client_id) {
    global $bdd;
    $reqSQL="SELECT * FROM api_client_grants
    WHERE active = 'Y' AND client_id = :id";

    $requete = $bdd->prepare($reqSQL);
    $requete->bindValue(':id', $client_id, PDO::PARAM_STR);      
    $requete->execute();

    $active = $requete->fetchAll();
    $requete->closeCursor();

    return $active;
}

// afficher les structures désactivés

function MODELE_getInactive($client_id) {
    global $bdd;
    $reqSQL="SELECT * FROM api_client_grants
    WHERE active = 'N' AND client_id = :id";

    $requete = $bdd->prepare($reqSQL);
    $requete->bindValue(':id', $client_id, PDO::PARAM_STR);      
    $requete->execute();

    $inactive = $requete->fetchAll();
    $requete->closeCursor();

    return $inactive;
}

//ajouter une structure 
function MODELE_insertStructure($client_id, $install_id, $active, $perms, $branch_id)
{
    global $bdd;
        
    $reqSQL='
        INSERT INTO api_client_grants (client_id, install_id, active, perms, branch_id)
        VALUES(:client_id, :install_id, :active, :perms, :branch_id)
    ';

    

    $requete = $bdd->prepare($reqSQL);
    $requete->bindValue(':client_id', $client_id, PDO::PARAM_STR);
    $requete->bindValue(':install_id', $install_id, PDO::PARAM_INT);
    $requete->bindValue(':active', $active, PDO::PARAM_STR);
    $requete->bindValue(':perms', $perms, PDO::PARAM_STR);
    $requete->bindValue(':branch_id', $branch_id, PDO::PARAM_INT);
    $requete->execute();    
    $requete->closeCursor();
}

// active / désactive une structure en cours


 function MODELE_updateactive($active,$id,$install) {
     global $bdd;
     $reqSQL = "UPDATE api_client_grants SET active = :active WHERE  branch_id = :branch_id AND install_id = :install_id";

     $requete = $bdd->prepare($reqSQL);
     $requete->bindValue(':active', $active, PDO::PARAM_STR);
     $requete->bindValue(':branch_id', $id, PDO::PARAM_INT);  
     $requete->bindValue(':install_id', $install, PDO::PARAM_INT);         
     $requete->execute();
   
     $requete->closeCursor();

    
 }

 function MODELE_update_grants($perms,$branch,$install) {
    global $bdd;
    $reqSQL = "UPDATE api_client_grants SET perms = :perms WHERE  install_id = :install_id AND branch_id = :branch_id";

    $requete = $bdd->prepare($reqSQL);
    $requete->bindValue(':perms', $perms, PDO::PARAM_STR);
    $requete->bindValue(':branch_id', $branch, PDO::PARAM_INT);  
    $requete->bindValue(':install_id', $install, PDO::PARAM_INT);         
    $requete->execute();
  
    $requete->closeCursor();

   
}


function insertToken($install_id, $branch_id, $t){
    global $bdd;
    
    
    $req=$bdd->prepare('insert into api_client_token (install_id, branch_id, token_secret) values(:install_id, :branch_id, :token_secret)');
    $req->bindValue(':install_id', $install_id, PDO::PARAM_INT);
    $req->bindValue(':branch_id', $branch_id, PDO::PARAM_INT);
    $req->bindValue(':token_secret', $t, PDO::PARAM_INT);
    $req->execute();    
    $req->closeCursor();
    return $t;
}

function getByToken($t){
    global $bdd;
    
    
    $req=$bdd->prepare('SELECT * FROM api_client_token WHERE token_secret = :t');
    $req->bindValue(':t', $t, PDO::PARAM_STR);
    $req->execute();    
    $result = $req->fetch();
    $req->closeCursor();
    return $result;
}