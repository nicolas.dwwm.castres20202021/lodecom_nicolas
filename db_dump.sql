-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: lodecom_sample
-- ------------------------------------------------------
-- Server version	8.0.23-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `lodecom_sample`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `lodecom_sample` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `lodecom_sample`;

--
-- Table structure for table `api_client`
--

DROP TABLE IF EXISTS `api_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `api_client` (
  `client_id` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `client_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `client_secret` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `active` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Y',
  `short_description` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `full_description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `logo_url` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `url` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `dpo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `technical_contact` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `commercial_contact` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `default_perms` text CHARACTER SET latin1 DEFAULT '{"members:read":true,"members:write":true,"members:add":true, "members-payment-schedules:read":true, "payment-schedules:read":true, "payment-schedules:write":true }',
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_client`
--

LOCK TABLES `api_client` WRITE;
/*!40000 ALTER TABLE `api_client` DISABLE KEYS */;
INSERT INTO `api_client` VALUES ('abc1234','Bob le Partenaire','31f7a65e315586ac198bd798b6629ce4903d0899476d5741a9f32e2e521b6a66','Y','Lorem ipsum dolor sit amet, consectetur adipiscing elit.','Aenean vel placerat nisl. Phasellus eu arcu sodales eros porttitor fringilla ac sit amet tortor. Morbi nec congue odio. Phasellus orci libero, feugiat at scelerisque a, dignissim eget massa.','https://www.bemovepro.com/wp-content/themes/BeMoveTheme/ressources/src/hand.png','https://bob-partenaire.fr/','dpo-example@lodecom.fr','commercial-example@lodecom.fr','tech-example@lodecom.fr',''),('def234','Toto le partenaire','cce66316b4c1c59df94a35afb80cecd82d1a8d91b554022557e115f5c275f515','Y','Aenean pharetra elit quis nunc sollicitudin faucibus.','Vivamus mollis metus egestas auctor molestie. Maecenas turpis lorem, interdum quis sagittis ac, aliquam sit amet ligula.','https://www.bemovepro.com/wp-content/themes/BeMoveTheme/ressources/src/hand.png','https://toto-partenaire.fr','dpo-example@lodecom.fr','commercial-example@lodecom.fr','tech-example@lodecom.fr',''),('klm456','Youpi partenaire','043d6c5097ea53ca880fbbfdb22451e80b07dda8901591a6574c99907a2e278e','Y','Sed pulvinar finibus risus, vitae sollicitudin dui rhoncus dapibus. Suspendisse cursus felis non laoreet porttitor.','Suspendisse viverra mi non elit maximus, vitae ultrices nunc rutrum. Nulla sodales sollicitudin sem, sit amet sollicitudin massa viverra nec. Phasellus tempus quam sit amet dolor lobortis, vitae faucibus turpis tincidunt. Nulla non viverra ex.','https://www.bemovepro.com/wp-content/themes/BeMoveTheme/ressources/src/hand.png','https://youpi-partenaire.fr','dpo-example@lodecom.fr','commercial-example@lodecom.fr','tech-example@lodecom.fr',''),('nop567','Yolo partenaire','f57d7c9dbaf3861c00a4a6a724fe8b5383cb211afe1a9993c15409a4ca14ec7f','Y','Vestibulum tristique ipsum id dui cursus fermentum. Pellentesque dictum libero aliquet lectus porta, non semper lorem gravida.','Vivamus in commodo tellus. Donec nec est lacus. Nam et mattis mauris. Etiam vitae bibendum massa. Phasellus posuere enim urna, ac tempus arcu faucibus at. In hac habitasse platea dictumst.','https://www.bemovepro.com/wp-content/themes/BeMoveTheme/ressources/src/hand.png','htpps://yolo-partenaire.fr','dpo-example@lodecom.fr','commercial-example@lodecom.fr','tech-example@lodecom.fr',''),('qrs678','Yes partenaire','f628421129af20c80d472e7a3eda41f794aae8c9b9afc005c911ec9e6eeb0b67','Y','Cras lobortis, nibh quis hendrerit maximus, tellus urna.','Curabitur ornare, neque eleifend rhoncus posuere, enim metus tempus risus, non condimentum tellus sapien sed augue. Aenean consectetur, ligula eleifend porta condimentum, metus quam eleifend arcu.','https://www.bemovepro.com/wp-content/themes/BeMoveTheme/ressources/src/hand.png','https://yes-partenaire.fr','dpo-example@lodecom.fr','commercial-example@lodecom.fr','tech-example@lodecom.fr',''),('qsd5678','Kevin le Partenaire','d1c7c99c6e2e7b311f51dd9d19161a5832625fb21f35131fba6da62513f0c099','Y','Lorem ipsum dolor sit amet, consectetur adipiscing elit.','Aenean vel placerat nisl. Phasellus eu arcu sodales eros porttitor fringilla ac sit amet tortor. Morbi nec congue odio. Phasellus orci libero, feugiat at scelerisque a, dignissim eget massa.','https://www.bemovepro.com/wp-content/themes/BeMoveTheme/ressources/src/hand.png','https://kevin-partenaire.fr/','dpo-example@lodecom.fr','commercial-example@lodecom.fr','tech-example@lodecom.fr',''),('tuv789','Super Partenaire','00d14b820a46a373dd551783a355f50c19e7997032fcc7e9607eeb7d86d51c85','N','Etiam blandit ipsum non lorem sodales, ac ornare tellus ultricies.','Pellentesque facilisis, nibh sed fringilla scelerisque, turpis erat congue libero, eget tristique nunc arcu ac diam. Sed massa dui, posuere aliquet ipsum id, ultricies auctor libero. Integer et luctus augue.','https://www.bemovepro.com/wp-content/themes/BeMoveTheme/ressources/src/hand.png','https://super-partenaire.fr','dpo-example@lodecom.fr','commercial-example@lodecom.fr','tech-example@lodecom.fr',''),('wxy890','Tulipe partenaire','0f0ba471a97a7f32b011eb1c55e3ae0b9d1a1c5ab738a5369722f4bade0b26f0','N','Curabitur sapien sem, vulputate malesuada consectetur vitae.','Etiam pulvinar lacus nec felis placerat dictum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Pellentesque a eleifend ante. Pellentesque at felis argt.','https://www.bemovepro.com/wp-content/themes/BeMoveTheme/ressources/src/hand.png','https://tulipe-partenaire.fr','dpo-example@lodecom.fr','commercial-example@lodecom.fr','tech-example@lodecom.fr',''),('zab112','Chouette partenaire','06507bb0ceed9041e21956ce5f36ff6660a12e6d1f0f1c250ade2921a73bb073','N','Proin iaculis, sem et luctus finibus, orci nibh ullamcorper felis','Cras sollicitudin, eros sed volutpat volutpat, velit justo laoreet elit, et convallis enim mi in est. Nullam mollis erat justo, eu euismod arcu hendrerit nec.','https://www.bemovepro.com/wp-content/themes/BeMoveTheme/ressources/src/hand.png','https://chouette-partenaire.fr','dpo-example@lodecom.fr','commercial-example@lodecom.fr','tech-example@lodecom.fr','');
/*!40000 ALTER TABLE `api_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `api_client_grants`
--

DROP TABLE IF EXISTS `api_client_grants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `api_client_grants` (
  `client_id` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `install_id` int NOT NULL,
  `active` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Y',
  `perms` text CHARACTER SET latin1 NOT NULL,
  `branch_id` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`install_id`,`branch_id`),
  KEY `client_id` (`client_id`),
  CONSTRAINT `api_client_grants_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `api_client` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_client_grants`
--

LOCK TABLES `api_client_grants` WRITE;
/*!40000 ALTER TABLE `api_client_grants` DISABLE KEYS */;
INSERT INTO `api_client_grants` VALUES ('zab112',-66,'Y','{\"members:read\":true,\"members:write\":true,\"members:add\":true, \"members-payment-schedules:read\":true, \"members-products:read\":true, \"payment-schedules:read\":true, \"payment-schedules:write\":true }',8),('wxy890',-65,'Y','{\"members:read\":true,\"members:write\":true,\"members:add\":true, \"members-payment-schedules:read\":true, \"members-products:read\":true, \"payment-schedules:read\":true, \"payment-schedules:write\":true }',7),('tuv789',-64,'Y','{\"members:read\":true,\"members:write\":true,\"members:add\":true, \"members-payment-schedules:read\":true, \"members-products:read\":true, \"payment-schedules:read\":true, \"payment-schedules:write\":true }',6),('nop567',-63,'Y','{\"members:read\":true,\"members:write\":true,\"members:add\":true, \"members-payment-schedules:read\":true, \"members-products:read\":true, \"payment-schedules:read\":true, \"payment-schedules:write\":true }',4),('klm456',-62,'Y','{\"members:read\":true,\"members:write\":true,\"members:add\":true, \"members-payment-schedules:read\":true, \"payment-schedules:read\":true, \"payment-schedules:write\":true }',3),('def234',-61,'Y','{\"members:read\":true,\"members:write\":true,\"members:add\":true, \"members-payment-schedules:read\":true, \"payment-schedules:read\":true, \"payment-schedules:write\":true }',2),('qsd5678',-60,'Y','{\"members:read\":true,\"members:write\":true,\"members:add\":true, \"members-payment-schedules:read\":true, \"members-products:read\":true, \"payment-schedules:read\":true, \"payment-schedules:write\":true }',1),('abc1234',-59,'Y','{\"members:read\":true,\"members:write\":true,\"members:add\":true, \"members-payment-schedules:read\":true, \"members-products:read\":true, \"payment-schedules:read\":true, \"payment-schedules:write\":true }',0),('qrs678',-58,'Y','{\"members:read\":true,\"members:write\":true,\"members:add\":true, \"members-payment-schedules:read\":true, \"members-products:read\":true }',5),('abc1234',-57,'N','{\"members:read\":true,\"members:write\":true,\"members:add\":true, \"members-payment-schedules:read\":true, \"members-products:read\":true, \"payment-schedules:read\":true, \"payment-schedules:write\":true }',0);
/*!40000 ALTER TABLE `api_client_grants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `api_client_perms`
--

DROP TABLE IF EXISTS `api_client_perms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `api_client_perms` (
  `install_id` int NOT NULL,
  `members_read` tinyint(1) DEFAULT NULL,
  `members_write` tinyint(1) DEFAULT NULL,
  `members_add` tinyint(1) DEFAULT NULL,
  `members_products_add` tinyint(1) DEFAULT NULL,
  `members_payment_schedules_read` tinyint(1) DEFAULT NULL,
  `members_statistiques_read` tinyint(1) DEFAULT NULL,
  `members_subscription_read` tinyint(1) DEFAULT NULL,
  `payment_schedules_read` tinyint(1) DEFAULT NULL,
  `payment_schedules_write` tinyint(1) DEFAULT NULL,
  `payment_day_read` tinyint(1) DEFAULT NULL,
  `branch_id` int NOT NULL,
  PRIMARY KEY (`install_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_client_perms`
--

LOCK TABLES `api_client_perms` WRITE;
/*!40000 ALTER TABLE `api_client_perms` DISABLE KEYS */;
INSERT INTO `api_client_perms` VALUES (-66,1,1,1,1,1,1,1,0,0,1,8),(-65,1,1,1,1,1,1,1,1,0,1,7),(-64,1,1,1,1,1,1,1,1,1,1,6),(-63,1,1,1,1,1,1,1,0,0,0,4),(-62,1,1,1,1,0,1,1,0,0,0,3),(-61,1,1,1,1,0,1,1,0,0,0,2),(-60,1,1,1,1,1,0,0,1,1,0,1),(-59,1,1,1,1,1,0,0,1,1,0,0),(-58,1,1,1,1,1,0,0,0,0,0,5);
/*!40000 ALTER TABLE `api_client_perms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `api_client_token`
--

DROP TABLE IF EXISTS `api_client_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `api_client_token` (
  `install_id` int NOT NULL,
  `branch_id` int NOT NULL DEFAULT '0',
  `token_secret` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_client_token`
--

LOCK TABLES `api_client_token` WRITE;
/*!40000 ALTER TABLE `api_client_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `api_client_token` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-04 10:51:41
