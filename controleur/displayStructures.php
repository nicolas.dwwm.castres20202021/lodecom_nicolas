<?php

// MODELE : inclusion des fonctions //////////////////////////////////////////////
include_once('model/modelStructure.php');
include_once('model/modelePartner.php');

if (isset($_POST['activer'])) {
	MODELE_getPartnerclientswitch($_POST['activer'], $_GET['client_id']);
}

if (isset($_POST['active_struct'])) {
	MODELE_updateactive($_POST['active_struct'], $_POST['branch_id'], $_POST['install_id']);
}

if (isset($_POST['input_perms'])) {
	MODELE_update_grants($_POST['input_perms'], $_POST['branch_id'], $_POST['install_id']);
}

if (isset($_GET['client_id'])) {
	$err = "";
	$data = new stdClass();
	$data->partenaire = MODELE_getPartnerclientid($_GET['client_id']);
	if (isset($_GET['filter'])) {
		if ($_GET['filter'] == 'active') {
			$data->structures = MODELE_getActive($_GET['client_id']);
		} else if ($_GET['filter'] == 'inactive') {
			$data->structures = MODELE_getInactive($_GET['client_id']);
		} else if ($_GET['filter'] == 'all') {
			$data->structures = MODELE_getstructureclientid($_GET['client_id']);
		}
	} elseif (isset($_GET['search'])) {
		if (strpos($_GET['search'], "-")) {
			$search = explode("-", $_GET['search']);
			$data->structures = MODELE_getBranch($search[0], "-" . $search[1], $_GET['client_id']);
			if (count($data->structures) <= 0) {
				$err = "aucune structure trouvée";
			}
		} else {
			if ($_GET['search'][0] == "-") {
				$data->structures = MODELE_getInstall($_GET['search'], $_GET['client_id']);
				if (count($data->structures) <= 0) {
					$err = "aucune structure trouvée";
				}
			} else {
				$data->structures = MODELE_getInstall("-" . $_GET['search'], $_GET['client_id']);
				if (count($data->structures) <= 0) {
					$err = "aucune structure trouvée";
				}
			}
		}
	} else {
		$data->structures = MODELE_getstructureclientid($_GET['client_id']);
	}
} else {
	$err = "Ce partenaire n'existe pas";
}

// VUE : On affiche la page /////////////////////////////////////////////////////////////

include('vue/allStructure.php');
